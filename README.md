# CallableResolver

A tool to resolve callables and their arguments

## Installation

install via composer
```json
    "require": {
        "dariorieke/callable-resolver": "dev-master"
    }
```

## Tests

run tests with the following command inside the repositories root:
```
./vendor/bin/phpunit  .\tests
```

## Usage

### Resolving callables

To resolve abstract callables to actual callables, use the `resolve` method. If the callable resolver fails to resolve the callable an `CallableResolverExceptionInterface` will be thrown:
```php
use DarioRieke\CallableResolver\CallableResolver;
use DarioRieke\CallableResolver\Exception\CallableResolverExceptionInterface;

$callableResolver = new CallableResolver();


$abstractCallable = 'mySpecialFunction';

try {
    $callable = $callableResolver->resolveCallable($abstractCallable);
    call_user_func($callable);
} 
catch (CallableResolverExceptionInterface $e) {
    //could not resolve the callable
}
```

Possible abstract callables which can be resolved:
 ```php
//closure
$abtractCallable1 = function() {};
//object without constructor arguments
//the callable resolver will create an instance of it 
$abtractCallable2 = ['SplQueue', 'rewind'];
//initialized object, regular callable
$abtractCallable3 = [new \SplQueue, 'rewind'];
// function name
$abtractCallable4 = 'phpinfo';
//static class method
$abtractCallable5 =  [TestClass::class, 'staticMethod'];
//static class method as string
$abtractCallable6 =  'TestClass::staticMethod';
 ```
If you pass in a `PSR-11 Container` into the `CallableResolver`s constructor, the resolver will try to gather any uninitialized class from the container. If no container is provided, the class will be initialized with `new $class()`, so constructor arguments are only possible in conjunction with a DI Container.  

 ### Resolving arguments

 > Note that currently you have to provide a `PSR-7` request implementation to use the ArgumentResolver.

To resolve the arguments for your callables, use the `ArgumentResolver`. 
Its main functionality is to resolve arguments from `PSR-11` containers for dependency injection and arguments from `PSR-7` server requests.

The resolver resolves arguments with different argument provider strategies which are passed via the constructor. This way you can add your own argument providers.


```php
use DarioRieke\CallableResolver\ArgumentResolver;
use DarioRieke\CallableResolver\ArgumentResolver\RequestArgumentProvider;
use DarioRieke\CallableResolver\ArgumentResolver\RequestAttributeArgumentProvider;
use DarioRieke\CallableResolver\ArgumentResolver\ServiceArgumentProvider;


$argumentResolver = new ArgumentResolver (
    //resolves the currently handled PSR-7 request
    new RequestArgumentProvider(),
    //resolves attributes from the PSR-7 request 
    new RequestAttributeArgumentProvider(), 
    //resolves arguments via the provided PSR-11 container 
    new ServiceArgumentProvider(/** PSR-11 implementation*/ $container),
);
```

#### Example of Argument Resolving:
```php
/**
 * type hint the arguments to let the ArgumentRresolver autoresolve them
 * works with RequestInterface and Interfaces and Classes registered in the DependencyInjectionContainer 
 * @param  string                $task    argument from the PSR-7 ServerRequest 
 * @param  int                   $id      argument from the PSR-7 ServerRequest
 * @param  RequestInterface      $request injects the current request
 * @param  LoggerInterface       $logger  a logger implementation resolved by type hint
 *                                        via PSR-11 container
 */
$callable = function (string $task, int $id, Psr\Http\Message\RequestInterface $request, LoggerInterface $logger) {
    // code
}

$args = $argumentResolver->resolveArguments($callable);

call_user_func_array($callable, $args);
```

#### Usage with `DarioRieke\DependencyInjection\DependencyInjectionContainerInterface`:
This interface extends the `PSR-11 ContainerInterface`. To resolve parameters from it, use the `ServiceParameterArgumentProvider` and pass in a `DependencyInjectionContainerInterface` implementation to its constructor. 
```php
	new ArgumentResolver (
		//...
		new ServiceParameterArgumentProvider($container)
	);
```
> Make sure you have installed the `"dariorieke/dependency-injection"` package to use this argument provider.
