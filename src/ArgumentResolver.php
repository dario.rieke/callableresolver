<?php 
namespace DarioRieke\CallableResolver;

use Psr\Http\Message\RequestInterface;
use DarioRieke\CallableResolver\ArgumentResolver\ArgumentProviderInterface;
use DarioRieke\CallableResolver\Exception\InvalidArgumentException;


/**
 * ArgumentResolverInterface
 */
class ArgumentResolver implements ArgumentResolverInterface {

	/**
	 * @var ParameterResolverInterface[]
	 */
	protected $resolvers = [];

	public function __construct(ArgumentProviderInterface ...$resolvers) {
		$this->resolvers = $resolvers;
	}

	public function resolveArguments(RequestInterface $request, callable $controller): array {
		if(is_array($controller)) {
			$method = new \ReflectionMethod($controller[0], $controller[1]);
		}
		elseif(is_string($controller) || $controller instanceof \Closure) {
			$method = new \ReflectionFunction($controller);
		}

		$params = $method->getParameters();
		$parameters = [];

		foreach ($params as $param) {
			$resolved = false;

			//try to load it with the ParameterResolvers via typehint
			foreach ($this->resolvers as $resolver) {
				if($resolver->supports($param, $request)) {
					$parameters[] = $resolver->resolve($param, $request);
					$resolved = true;
					//move to the next parameter
					continue 2;
				}
			}

			if(!$resolved) {
				throw new InvalidArgumentException("Could not resolve argument: {$param->getName()}");
			}
		}

		return $parameters;
	}
}

?>
