<?php 
namespace DarioRieke\CallableResolver\ArgumentResolver;
use Psr\Http\Message\RequestInterface;

/**
 * ArgumentProviderInterface
 * used to provide an argument via reflection
 */
interface ArgumentProviderInterface {

	/**
	 * check if the Resolver supports the given param
	 * @param  \ReflectionParameter  $param  information about the argument to resolve
	 * @return bool       					return a value for the given param
	 */
	public function supports(\ReflectionParameter $param, RequestInterface $request): bool;

	/**
	 * resolve the given param by returning a value for it
	 * @param  \ReflectionParameter  $param  information about the argument to resolve
	 * @return mixed        				return a value for the given param
	 */
	public function resolve(\ReflectionParameter $param, RequestInterface $request);
}

?>
