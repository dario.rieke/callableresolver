<?php 
namespace DarioRieke\CallableResolver\ArgumentResolver;

use Psr\Http\Message\RequestInterface;
use DarioRieke\CallableResolver\ArgumentResolver\ArgumentProviderInterface;

/**
 * RequestArgumentProvider
 * provides the currently handled PSR-7 request
 */
class RequestArgumentProvider implements ArgumentProviderInterface {

	public function supports(\ReflectionParameter $param, RequestInterface $request): bool {
		if($type = $param->getType()) {
			return($type->getName() === RequestInterface::class);
		}
		else return false;
	}

	public function resolve(\ReflectionParameter $param, RequestInterface $request) {
		return $request;
	}
}

?>
