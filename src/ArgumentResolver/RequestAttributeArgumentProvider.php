<?php 
namespace DarioRieke\CallableResolver\ArgumentResolver;

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\RequestInterface;
use DarioRieke\CallableResolver\ArgumentResolver\ArgumentProviderInterface;

/**
 * RequestAttributeArgumentProvider
 * provides an attribute from a PSR-7 request implementation
 */
class RequestAttributeArgumentProvider implements ArgumentProviderInterface {

	/**	
	 * the default value which will be returned as a default
	 * to check if an attribute value exists
	 */
	const NO_VALUE = 'QTvJunhVmvfwhhr94jNB';

	public function supports(\ReflectionParameter $param, RequestInterface $request): bool {
		if( !($request instanceof ServerRequestInterface) ) return false;
		
		return ($request->getAttribute($param->getName(), self::NO_VALUE) !== self::NO_VALUE);
	}

	public function resolve(\ReflectionParameter $param, RequestInterface $request) {
		return $request->getAttribute($param->getName());
	}
}

?>
