<?php 
namespace DarioRieke\CallableResolver\ArgumentResolver;

use Psr\Http\Message\RequestInterface;
use Psr\Container\ContainerInterface;
use DarioRieke\CallableResolver\ArgumentResolver\ArgumentProviderInterface;


/**
 * ServiceArgumentProvider
 * provides an argument from a PSR-11 container interface
 */
class ServiceArgumentProvider implements ArgumentProviderInterface {

	/**
	 * 
	 * @var ContainerInterface
	 */
	protected $container;

	/**
	 * inject the container
	 * @param ContainerInterface $container 
	 */
	public function __construct(ContainerInterface $container) {
		$this->container = $container;
	}

	public function supports(\ReflectionParameter $param, RequestInterface $request): bool {
		if($type = $param->getType()) {
			return $this->container->has($param->getType()->getName());
		}
		else return false;
	}

	public function resolve(\ReflectionParameter $param, RequestInterface $request) {
		return $this->container->get($param->getType()->getName());		
	}
}

?>
