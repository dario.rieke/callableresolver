<?php 
namespace DarioRieke\CallableResolver\ArgumentResolver;

use Psr\Http\Message\RequestInterface;
use DarioRieke\DependencyInjection\DependencyInjectionContainerInterface;
use DarioRieke\CallableResolver\ArgumentResolver\ArgumentProviderInterface;

/**
 * ServiceParameterArgumentProvider
 * provides a parameter from a DarioRieke\DependencyInjection\DependencyInjectionContainerInterface container
 */
class ServiceParameterArgumentProvider implements ArgumentProviderInterface {

	/**
	 * 
	 * @var DependencyInjectionContainerInterface
	 */
	protected $container;

	/**
	 * inject the dependency container
	 * @param DependencyInjectionContainerInterface $container 
	 */
	public function __construct(DependencyInjectionContainerInterface $container) {
		$this->container = $container;
	}

	public function supports(\ReflectionParameter $param, RequestInterface $request): bool {
		return $this->container->hasParameter($param->getName());
	}

	public function resolve(\ReflectionParameter $param, RequestInterface $request) {
		return $this->container->getParameter($param->getName());		
	}
}

?>
