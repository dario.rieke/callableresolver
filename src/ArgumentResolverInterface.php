<?php 
namespace DarioRieke\CallableResolver;

use Psr\Http\Message\RequestInterface;

/**
 * ArgumentResolverInterface
 * resolve arguments for a callable by returning an array in the right order
 */
interface ArgumentResolverInterface {
	/**
	 * resolve arguments for a callable by returning an array in the right order
	 * 
	 * @param Psr\Http\Message\RequestInterface  
	 * @param callable  			 $controller callable to return arguments for
	 * @throws DarioRieke\CallableResolver\Exception\CallableResolverExceptionInterface
	 * @return array				 example: [ 'Name', 2, true]
	 * 
	 */
	public function resolveArguments(RequestInterface $request, callable $controller): array;
}

?>
