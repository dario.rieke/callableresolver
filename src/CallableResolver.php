<?php 
namespace DarioRieke\CallableResolver;

use DarioRieke\CallableResolver\Exception\InvalidArgumentException;
use Psr\Container\ContainerInterface;

/**
 * CallableResolver
 */
class CallableResolver implements CallableResolverInterface {

	/**
	 * @var ContainerInterface
	 */
	private $container;

		
	/**
	 * create the CallableResolver
	 *
	 * @param  ContainerInterface|null $container
	 * @return void
	 */
	public function __construct( ContainerInterface $container = null) {
		$this->container = $container;
	}

	public function resolveCallable($controller): callable {
		//controller is already instanciated
		if(is_array($controller) && \is_object($controller[0]) && \is_string($controller[1])) {
			return $controller;
		}
		//controller is not instanciated yet
		elseif( is_array($controller) && is_string($controller[0]) && is_string($controller[1]) ) {
			try {
				$reflection = new \ReflectionClass($controller[0]);
			}
			catch(\ReflectionException $e) {
				throw new InvalidArgumentException("Callable could not be resolved. \"{$controller[0]}\" is not a class.");
			}
			$method = $reflection->getMethod($controller[1]);
			if($method->isStatic()) {
				if(is_callable($controller)) {
					return $controller;
				}
				else {
					throw new InvalidArgumentException("Callable could not be resolved.");
				}
			}
			else {
				$callable = [$this->instantiateController($controller[0]), $controller[1] ];
				if(is_callable($callable)) {
					return $callable;
				}
				else {
					throw new InvalidArgumentException("Callable could not be resolved.");
				}
				
			}
		}
		//controller is a closure
		elseif($controller instanceof \Closure) {
			return $controller;
		}
		//controller is a function name
		elseif(is_string($controller)) {
			if(is_callable($controller)) {
				return $controller;
			}
			else {
				throw new InvalidArgumentException("Callable could not be resolved.");
			}
		}
		else {
			throw new InvalidArgumentException("Callable could not be resolved.");
		}
	}

	/**
	 * create a new instance of a given Class
	 * @param  class    $class the class to instantiate
	 * @return object          the instantiated object
	 */
	protected function instantiateController($class): object {
		if($this->container && $this->container->has($class)) {
			return $this->container->get($class);
		}
		return new $class();
	}
}
?>