<?php 
namespace DarioRieke\CallableResolver;


/**
 * resolve an abstract callable to an actual callable 
 */
interface CallableResolverInterface {
	/**
	 * resolves $callable to an actual callable
	 * @param  callable|string|array $callable  callable to resolve
	 * @throws DarioRieke\CallableResolver\Exception\CallableResolverExceptionInterface
	 * @return callable              			an actual callable
	 */
	public function resolveCallable($callable): callable;
}
?>