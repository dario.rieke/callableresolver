<?php 
namespace DarioRieke\CallableResolver\Exception;

use DarioRieke\CallableResolver\Exception\CallableResolverExceptionInterface;

/**
 * InvalidArgumentException
 */ 
class InvalidArgumentException extends \InvalidArgumentException implements CallableResolverExceptionInterface {

}
