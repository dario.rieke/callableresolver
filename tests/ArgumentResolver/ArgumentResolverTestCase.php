<?php 
namespace DarioRieke\CallableResolver\Tests\ArgumentResolver;

use PHPUnit\Framework\TestCase;
use DarioRieke\CallableResolver\ArgumentResolver\ArgumentProviderInterface;
use Psr\Http\Message\RequestInterface;
use \ReflectionParameter;


/** 
 * TestCase for ArgumentResolvers
 */
abstract class ArgumentResolverTestCase extends TestCase {

	public function testImplementsArgumentProviderInterface() {
		$this->assertInstanceOf(ArgumentProviderInterface::class, $this->getResolver());
    }

  public function getRequestMock() {
    return $this->createMock(RequestInterface::class);
  }

  /** 
   * test for the resolver
   */
  public abstract function testSupportsAndResolvesArgument();

  /** 
   * test for the resolver 
   */
  // public abstract function testDoesNotSupportNonSupportedArgument();

  /** 
   * used to get the currently tested resolver
   */
  public abstract function getResolver(): ArgumentProviderInterface;
}
