<?php 
namespace DarioRieke\CallableResolver\Tests\ArgumentResolver;

use DarioRieke\CallableResolver\ArgumentResolver\RequestArgumentProvider;
use DarioRieke\CallableResolver\Tests\ArgumentResolver\ArgumentResolverTestCase;
use DarioRieke\CallableResolver\ArgumentResolver\ArgumentProviderInterface;
use Psr\Http\Message\RequestInterface;




class RequestArgumentProviderTest extends ArgumentResolverTestCase {

    public function getResolver(): ArgumentProviderInterface {
        return new RequestArgumentProvider();
    }

	public function testImplementsArgumentProviderInterface() {
		$this->assertInstanceOf(ArgumentProviderInterface::class, $this->getResolver());
    }

    public function testSupportsAndResolvesArgument() {
        $resolver = $this->getResolver();
        $request = $this->getRequestMock();
        
        $testFunction = function(RequestInterface $request) { };
        $reflection = new \ReflectionFunction($testFunction);
        $reflectionParameter = $reflection->getParameters()[0];

        $this->assertTrue($resolver->supports($reflectionParameter, $request));

        $this->assertSame($request, $resolver->resolve($reflectionParameter, $request));
    }

    // /** 
    //  * @dataProvider getNonSupportedArguments
    //  */
    // public function testDoesNotSupportNonSupportedArgument($callable) {
    //     $resolver = $this->getResolver();
    //     $request = $this->getRequestMock();

    //     $testFunction = function($someArgument) {};
    //     //not ready here!!!
    //     $this->assertFalse($resolver->supports($test));
    // }

    // public function getNonSupportedReflectionParameters() {
    //     foreach($functions as $function) {
    //         $reflection = new \ReflectionFunction($function);
    //         $param = $reflection->get
    //     }
    // }
}
