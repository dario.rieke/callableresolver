<?php 
namespace DarioRieke\CallableResolver\Tests\ArgumentResolver;

use DarioRieke\CallableResolver\ArgumentResolver\RequestAttributeArgumentProvider;
use DarioRieke\CallableResolver\Tests\ArgumentResolver\ArgumentResolverTestCase;
use DarioRieke\CallableResolver\ArgumentResolver\ArgumentProviderInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ServerRequestInterface;


class RequestAttributeArgumentProviderTest extends ArgumentResolverTestCase {

    public function getResolver(): ArgumentProviderInterface {
        return new RequestAttributeArgumentProvider();
    }

    public function getRequestMock() {
        return $this->createMock(ServerRequestInterface::class);
    }

    public function testSupportsAndResolvesArgument() {
        $resolver = $this->getResolver();
        $request = $this->getRequestMock();
        $request->method("getAttribute")->will(
            $this->returnValueMap(
                [
                    [ 'testAttribute', 'value' ],
                ]
            )
        );
        
        $testFunction = function($testAttribute) { };
        $reflection = new \ReflectionFunction($testFunction);
        $reflectionParameter = $reflection->getParameters()[0];

        $this->assertTrue($resolver->supports($reflectionParameter, $request));

        $this->assertSame($request->getAttribute('testAttribute'), $resolver->resolve($reflectionParameter, $request));
    }
}
