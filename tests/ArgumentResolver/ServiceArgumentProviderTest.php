<?php 
namespace DarioRieke\CallableResolver\Tests\ArgumentResolver;

use DarioRieke\CallableResolver\ArgumentResolver\ServiceArgumentProvider;
use DarioRieke\CallableResolver\Tests\ArgumentResolver\ArgumentResolverTestCase;
use DarioRieke\CallableResolver\ArgumentResolver\ArgumentProviderInterface;
use Psr\Container\ContainerInterface;


class ServiceArgumentProviderTest extends ArgumentResolverTestCase {

    public function getResolver(): ArgumentProviderInterface {
        return new ServiceArgumentProvider($this->getContainerMock());
    }

    public function getContainerMock() {
        $container = $this->createMock(ContainerInterface::class);
        $container->method("has")->will(
            $this->returnValueMap(
                [
                    [ \SplQueue::class, true ],
                ]
            )
        );
        $container->method("get")->will(
            $this->returnValueMap(
                [
                    [ \SplQueue::class, new \SplQueue() ],
                ]
            )
        );
        return $container;
    }

    public function testSupportsAndResolvesArgument() {
        $resolver = $this->getResolver();
        $request = $this->getRequestMock();
        $container = $this->getContainerMock();
        
        $testFunction = function(\SplQueue $testAttribute) { };
        $reflection = new \ReflectionFunction($testFunction);
        $reflectionParameter = $reflection->getParameters()[0];

        $this->assertTrue($resolver->supports($reflectionParameter, $request));

        $this->assertEquals($container->get(\SplQueue::class), $resolver->resolve($reflectionParameter, $request));
    }
}
