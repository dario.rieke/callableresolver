<?php 
namespace DarioRieke\CallableResolver\Tests\ArgumentResolver;

use DarioRieke\CallableResolver\ArgumentResolver\ServiceParameterArgumentProvider;
use DarioRieke\CallableResolver\Tests\ArgumentResolver\ArgumentResolverTestCase;
use DarioRieke\CallableResolver\ArgumentResolver\ArgumentProviderInterface;
use DarioRieke\DependencyInjection\DependencyInjectionContainerInterface;



class ServiceParameterArgumentProviderTest extends ArgumentResolverTestCase {

    public function getResolver(): ArgumentProviderInterface {
        return new ServiceParameterArgumentProvider($this->getContainerMock());
    }

    public function getContainerMock() {
        $container = $this->createMock(DependencyInjectionContainerInterface::class);
        $container->method("hasParameter")->will(
            $this->returnValueMap(
                [
                    [ 'testAttribute', true ],
                ]
            )
        );
        $container->method("getParameter")->will(
            $this->returnValueMap(
                [
                    [ 'testAttribute', 'value' ],
                ]
            )
        );
        return $container;
    }

    public function testSupportsAndResolvesArgument() {
        $resolver = $this->getResolver();
        $request = $this->getRequestMock();
        $container = $this->getContainerMock();
        
        $testFunction = function($testAttribute) { };
        $reflection = new \ReflectionFunction($testFunction);
        $reflectionParameter = $reflection->getParameters()[0];

        $this->assertTrue($resolver->supports($reflectionParameter, $request));

        $this->assertSame($container->getParameter('testAttribute'), $resolver->resolve($reflectionParameter, $request));
    }
}
