<?php 
namespace DarioRieke\CallableResolver\Tests;

use PHPUnit\Framework\TestCase;
use DarioRieke\CallableResolver\Tests\Fixtures\TestClass;
use DarioRieke\CallableResolver\ArgumentResolver;
use DarioRieke\CallableResolver\ArgumentResolverInterface;
use DarioRieke\CallableResolver\Exception\CallableResolverExceptionInterface;
use DarioRieke\CallableResolver\ArgumentResolver\ArgumentProviderInterface;
use Psr\Http\Message\RequestInterface;


class ArgumentResolverTest extends TestCase {

    public function getArgumentResolverWithProviders($valueToReturn) {
        $argumentProvider1 = $this->getArgumentProviderMock();
        $argumentProvider1->expects($this->any())
                          ->method('supports')
                          ->will($this->returnValue(false));

        $argumentProvider2 = $this->getArgumentProviderMock();
        $argumentProvider2->expects($this->any())
                          ->method('supports')
                          ->will($this->returnValue(false));

        $argumentProvider3 = $this->getArgumentProviderMock();
        $argumentProvider3->expects($this->any())
                          ->method('supports')
                          ->will($this->returnValue(false));

        //this one will resolve all the arguments
        $argumentProvider3 = $this->getArgumentProviderMock();
        $argumentProvider3->expects($this->any())
                          ->method('supports')
                          ->will($this->returnValue(true));

        $argumentProvider3->expects($this->any())
                          ->method('resolve')
                          ->will($this->returnValue($valueToReturn));
            

        return new ArgumentResolver(
            $argumentProvider1, 
            $argumentProvider2,
            $argumentProvider3
        );
    }

    public function getArgumentResolver() {
        return new ArgumentResolver();
    }

    public function getArgumentProviderMock() {
        return $this->createMock(ArgumentProviderInterface::class);
    }

    public function getRequestMock() {
        return $this->createMock(RequestInterface::class);
    }

	public function testImplementsArgumentResolver() {
		$this->assertInstanceOf(ArgumentResolverInterface::class, $this->getArgumentResolver());
    }

    public function testCallsArgumentProviders() {
        $resolver = $this->getArgumentResolverWithProviders('returnValue');
        $callable = function($someArgument, $someSecondArgument) {};
        $arguments = $resolver->resolveArguments($this->getRequestMock(), $callable);
        
        $this->assertSame(['returnValue', 'returnValue'], $arguments);
    }
    //maybe add data provider here to test order of arguments etc
}
