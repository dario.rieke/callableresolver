<?php 
namespace DarioRieke\CallableResolver\Tests;

use PHPUnit\Framework\TestCase;
use DarioRieke\CallableResolver\Tests\Fixtures\TestClass;
use DarioRieke\CallableResolver\CallableResolverInterface;
use DarioRieke\CallableResolver\CallableResolver;
use DarioRieke\CallableResolver\Exception\CallableResolverExceptionInterface;
use Psr\Container\ContainerInterface;


class CallableResolverTest extends TestCase {

	public function testImplementsCallableResolverInterface() {
		$this->assertInstanceOf(CallableResolverInterface::class, $this->getResolver());
    }

    /**
     * @dataProvider getAbstractCallables
     */
    public function testResolvesAbstractCallablesWithoutContainer($abstractCallable) {
        $resolver = $this->getResolver();
        $result = $resolver->resolveCallable($abstractCallable);
        $this->assertTrue(\is_callable($result));
    }

    /**
     * @dataProvider getInvalidAbstractCallables
     */
    public function testThrowsExceptionIfNotResolvable($invalidAbstractCallable) {
        $resolver = $this->getResolver();
        $this->expectException(CallableResolverExceptionInterface::class);
        $result = $resolver->resolveCallable($invalidAbstractCallable);
        
    }

    public function testResolvesAbstractCallableFromContainer() {
        $expectedInstance = new TestClass();
        $containerMock = $this->createMock(ContainerInterface::class);

        $containerMock->expects($this->once())->method('has')->will(
            $this->returnValueMap(
                [
                    [Testclass::class, true]
                ]
            )
        );
        $containerMock->expects($this->once())->method('get')->will(
            $this->returnValueMap(
                [
                    [Testclass::class, $expectedInstance]
                ]
            )
        );

        $resolver = $this->getResolver($containerMock);

        $result = $resolver->resolveCallable([Testclass::class, 'someMethod']);

        $this->assertTrue(\is_callable($result));
        $this->assertSame($expectedInstance, $result[0]);
    }

    public function getAbstractCallables() {
        return [
            'closure'                               => [function() {}],
            //we use SplQueue as it has no constructor arguments
            'array (uninitialized)'                 => [ ['SplQueue', 'rewind'] ],
            'array (initialized)'                   => [ [new \SplQueue, 'rewind'] ],
            'function name'                         => [ 'phpinfo' ],
            'static method (uninitialized)'         => [ [TestClass::class, 'staticMethod'] ],
            'static method (initialized)'           => [ [$this->getTestClass(), 'staticMethod'] ],
            'static method (string)'                => [ Testclass::class.'::'.'staticMethod' ],
        ];
    }

    public function getInvalidAbstractCallables() {
        return [
            'int'     => [ 2432 ],
            'bool'    => [ true ],
            'string'  => [ 'thisFunctionDoesNotExist' ],
            'array'   => [ [ 'this', 'is', 'not', 'a', 'callable!' ] ]
        ];
    }
    
    protected function getResolver(ContainerInterface $container = null) {
        return new CallableResolver($container);
    }

    protected function getTestClass() {
        return new TestClass;
    }
}
