<?php 
namespace DarioRieke\CallableResolver\Tests\Fixtures;

/** 
 * class used for testing with a static method
 */
class TestClass {
    static function staticMethod() {
        
    }
    public function someMethod() {
        
    }
}

